# How to access the Hadoop cluster

### 1. Preparing

If you are on Linux add the following lines to `/etc /hosts`:
```
127.0.0.1       sber-master.atp-fivt.org sber-master
127.0.0.1       sber-node01.atp-fivt.org sber-node01
127.0.0.1       sber-node02.atp-fivt.org sber-node02
127.0.0.1       sber-node03.atp-fivt.org sber-node03
```

If you are on Windows, install yourself [Git] (https://git-scm.com/downloads) go to Git Bash. This shell will enable you to execute commands in a Linux-like terminal.

### 2. Checking the access

For access with forwarding of the necessary ports, use the command:

```bash
ssh USER@sber-client.atp-fivt.org \
-L 8088:sber-master:8088 \
-L 8889:sber-master:8889 \
-L 19888:sber-master:19888 \
-L 18088:sber-master:18088 \
-L 8888:sber-node01:8888 \
-L 9870:sber-master:9870 \
-L 8042:sber-node01:8041 \
-L 8043:sber-node02:8041 \
-L 8044:sber-node03:8041 \
-L 8045:sber-node01:8042 \
-L 8046:sber-node02:8042 \
-L 8047:sber-node03:8042
```

Instead of `USER`, use the login that came to your mail from automation@atp-fivt.org.
It is not necessary to forward all the ports. For the first lessons  such ports are enough:
```bash
ssh USER@sber-client.atp-fivt.org \
-L 8088:sber-master:8088 \
-L 19888:sber-master:19888 \
-L 8888:sber-node01:8888 \
-L 9870:sber-master:9870 \
```
