# Hive homework

* Deadline: 10.09, 16:59 BCN.

## Introduction
* There are 5 problems in this hw, the last of them does not contain tests. This means that in the harbourhivetask5 branch, you can ignore the "red" pipeline.
* Unlike HW by MapReduce, you do not need to redirect Hive logs to `/dev/null`.
* The rest of the principle of delivery of remote sensing is the same as in MapReduce.

## Initial data: user logs

The data is located in HDFS at `/data/user_logs/*_M`. They consist of three parts, each of which is located in its own subdirectory. The data in each part differs by the number and type of columns, separated by tabs ('\ t') or spaces.

#### A. Logs of user requests for news messages (user_logs).
1. Ip-address (STRING),
2. Request time (TIMESTAMP or INT),
3. An http request (STRING) from the ip-address,
4. The size of the page transmitted to the client (INT),
5. Http-status code (INT).
6. Information about the client application from which the request to the server was made, including information about the browser (STRING).

#### Important: information about the browser is contained in the beginning of the 6th field of the log (characters from zero position to the position of the first whitespace character), the content of the rest of the line is not determined by the user's browser. The delimiter between IP and request time has 3 tabs.

#### B. Information about users (user_data).
1.IP-address (STRING),
2. User's browser (STRING),
3. Gender (STRING) // male, female,
4. Age (INT).

#### С. Information about the location of users' IP addresses (ip_data).
1.IP-address (STRING),
2. Region (STRING).

## Preparing the code for delivery
For each home, there are pre-created repositories with a master branch and branches for each task in the home. To pass homework you need:

1. In each branch create a directory, directory name = branch name. For example, you need to create directories named `harbourhivetask1` and` harbourhivetask2` in the branches `harbourhivetask1` and` harbourhivetask2`, respectively.

2. Put the `run.sh` file in the created directories. This is the entry point into your program and it will be launched by the verification system. In `run.sh` there can be either the whole solution of the problem or a call to other files. In this case, `run.sh` will contain something like this:` hive -f query.sql`.

3. Logs and output, unlike the previous homework, do not need to be redirected to / dev / null.


## Tasks

### Task 1 (411). 
Create external (EXTERNAL) tables from the source data. As a result, there will be 4 tables: user logs, data of ip addresses, data of users and subnets. From the logs table, transfer the data to another table, partitioned by dates - one partition for each day. On partitioned tables, you will need to execute queries in the following tasks.

Data serialization and deserialization is required using regular expressions (see `org.apache.hadoop.hive.serde2.RegexSerDe`).

Check the correctness of creating tables using the simplest queries (`SELECT * FROM <table> LIMIT 10`). These Select queries must also be added to the task script.

#### Additional requirements:
1. For each of you, a database is reserved in the system, the name of which coincides with the login on ** GitLab.atp-fivt.org ** (for example, ** ivchenkoon **). Therefore, when uploading code for testing, write `USE <name_on_gitlab.atp-fivt.org>`. For debugging, create your base with any name.
2. Do not add the code for creating the database to the GitLab repository. the databases have already been created for you and the testing system does not have the right to overwrite these databases.
3. Tables should be named like this:
    * Logs - a partitioned table with logs.
    * Users - a table with information about users.
    * IPRegions - a table with IP and regions.
    * Subnets - table with subnets (`/data/subnets/variant1`).

* Example result: *
``,
33.49.147.163 http://lenta.ru/4303000 1189 451 Chrome / 5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident / 5.0) n 20140101
75.208.40.166 http://newsru.com/3330815 60 306 Safari / 5.0 (Windows; U; MSIE 9.0; Windows NT 8.1; Trident / 5.0; .NET4.0E; en-AU) n 20140101
``,

## Task 2. 
Write a request that selects the number of visits for each day. Sort the results in descending order.

* Example result: *
``,
20140308 96
20140409 96
20140318 96
``,
Because after data aggregation, it becomes a little, `LIMIT` does not need to be used.

## Task 3. 
Write a query that selects the number of visits from men and women by region.

* Example result: *
``,
Tver 66968157 29097223
Voronezh 60445347 26333509
``,
Hint for problem 3: use the [IF] construct (https://www.folkstalk.com/2011/11/conditional-functions-in-hive.html).

## Task 4. 
Imagine a situation where all news sites have moved to a .com domain. You were asked to update the log database so that user logs would point not to old domains, but to new ones. For example, the news link http://news.rambler.ru/8744806 should now look like http://news.rambler.com/8744806 in your queries. Use streaming in hive-sql query. (It is recommended to pay attention to the awk and sed commands). Print TOP-10 log entries without sorting.

* Example result: *
``,
49.203.96.67 20140102 http://lenta.com/2296722 716 499 Safari / 5.0
33.49.147.163 20140102 http://news.yandex.com/5605690 850 300 Safari / 5.0
``,
Objective 4 must be solved using Hive Streaming. Note that the output requires not only the field that we are changing, but all 6 fields.

## Task 5. 
Imagine that you have a lot of data, and you need to process this data as soon as possible. Write a request from the previous task 3 of your own variant, but using sampling ([TABLESAMPLE] (https://cwiki.apache.org/confluence/display/Hive/LanguageManual+Sampling)). Try different values ​​for the percentage of samples from the total and compare the accuracy of the estimates obtained depending on the percentage of samples. Display the result on the graph. Also commit the schedule to the branch with the task.

To assess accuracy, you can use any metric that is convenient for you. For comparison, you can take both the average values ​​for the regions, and separately taken regions.

There are no automated tests for this task, so ignore the crashing CI jobs.

Literature
1. Tom White. Hadoop: The Definitive Guide, 3rd edition. O'Reilly, 2012, chapter 12.
2. Edward Capriolo, Dean Wampler, Jason Rutherglen. Programming Hive. O'Reilly, 2012.
3. [UDTF Examples] (https://cwiki.apache.org/confluence/display/Hive/LanguageManual+UDF#LanguageManualUDF-Built-inTable-GeneratingFunctions%28UDTF%29) from Hive wiki.

Below are the points you get for each challenge.

| Task | Branch in the repository | Points |
|:- |:- |:- |
| 1 | harbourhivetask1 | 0.3 |
| 2 | harbourhivetask2 | 0.2 |
| 3 | harbourhivetask3 | 0.3 |
| 4 | harbourhivetask4 | 0.2 |
| 5 | harbourhivetask5 | 0.5 |

### Comments
1. In some tasks it is required to display TOP-N records. This simply means using LIMIT N. It is not necessary to sort the query result. Sorting is needed only in tasks 2.x.
2. Please send the 1st problem for testing first, and then all the others.
3. Testing of the rest of the tasks takes place on the tables that were created in the 1st task. If you did task 1 incorrectly, everyone else will not be tested either.
4. In some situations, your personal Hive database, on which testing takes place, can be corrupted by your own requests. In this case, fill out the [** form **] (https://forms.gle/MmaPNj9NGvqpNYYr6) to re-create it. This can be done in the course chat. 
