# Homework on Spark Advanced and Realtime

#### Comments 

* Task 1 can be performed on both the RDD and DF API. But it is better to use DF as a higher-level and modern one.
* Task 2 - only on the Steam API (analogous to RDP for Spark Streaming).

## Task 1. Spark advanced
#### Source data

* `/data/wiki/en_articles_part` - Wikipedia articles. You need to send it for testing on a partial dataset (so as not to overload the cluster).

Data format:
```
article ID <tab> article text
```

#### Task 

The task is to extract collocations. These are combinations of words that often occur together. For example, "High school" or "Roman Empire". To find matches, you need to use the NPMI metric (normalized point mutual information).

The PMI of two words a and b is defined as:
```math
\textcolor{blue}{PMI \lparen a,b \rparen = ln \bigg( {\frac {P(a,b)}{P(a)*P(b)}} \bigg)}
```
, where `P(ab)` - the probability of two consecutive words, and `P(a)` и `P (b)` - the probabilities of the words a and b, respectively.

You will need to estimate the probability of occurrence of words, that is:
```math
P(a)=\frac{num\_of\_occurrences\_of\_word\_"a"}{num\_of\_occurrences\_of\_all\_words}
```
```math
P(ab)=\frac{num\_of\_occurrences\_of\_pair\_"ab"}{num\_of\_occurrences\_of\_all\_pairs}
```

* *total_number_of_words* - total number of words in the text
* *total_number_of_word_pairs* - total number of pairs
* "Roman Empire"; let's assume that this is a unique combination, and each occurrence of" Roman "is followed by" Empire", and, conversely, each occurrence of" Empire "is preceded by"Roman". In this case «P (ab) = P (a) = P(b)», поэтому «PMI (a, b) = -lnP(a) = -lnP(b)». The rarer this collocation occurs, the greater the PMI value.
* "the doors"; let's assume that "the" can occur next to any word. Thus, «P (ab) = P (a) * P (b)» и «PMI (a, b) = ln 1 = 0".
* "green idea / sleeps furiously"; when two words never meet together, «P (ab) = 0» и «PMI (a, b) = -inf».
 
NPMI is defined as $`NPMI(a,b)=-\frac{PMI(a,b)}{lnP(a,b)}`$ . This normalizes the value in the range *[-1; 1]*.

Find the most popular collocations on Wikipedia. Data processing:
* When parsing, discard all characters that are not Latin letters: `text = re.sub("^\W+|\W+$", "", text)`
* we reduce all words to lowercase;
* we delete all stop words (even inside the bigram because “at evening "has the same semantics as “at the evening");
combine bigrams with the underscore"_";
* we work only with those charts that have been encountered at least 500 times (i.e. we perform all the necessary joins and calculate NPMI only for them).
* the total number of words and bigrams is counted before filtering.
 
For each bigram, calculate the NPMI and display (in STDOUT) the TOP-39 most popular collocations, sorted in descending order of the NPMI value. The NPMI value itself does not need to be output.

Output example:
```
roman_empire
south_africa
```
Example of output on a sample dataset (with values of NPMI):
```
19th_century	0.757464166177
20th_century	0.751460453349
references_external	0.731826941011
soviet_union	0.727806412183
air_force	0.705773204264
baseball_player	0.691711138551
university_press	0.687424532005
roman_catholic	0.683677693663
united_kingdom	0.68336461567
```
Hint: if you do everything correctly ,then "roman_empire" and "south_africa" will be in the response.

## Задача 2. Spark Streaming
#### Data
Input: `/data/realtime/uids`

Data format:
```
...
seg_firefox 4176
...
```

#### Task

A segment is a set of users defined by a certain attribute. When a user visits a web service from his device, this event is logged on the web service side in the following format ` 'user_id <tab> user_agent'. For example:
```
f78366c2cbed009e1febc060b832dbe4	Mozilla/5.0 (Linux; Android 4.4.2; T1-701u Build/HuaweiMediaPad) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.73 Safari/537.36
62af689829bd5def3d4ca35b10127bc5	Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36
```
Portions of web logs are received at the input in the described format. It is required to divide the audience (users) in these logs into the following segments:
1. Users who work on the Internet from under the iPhone.
2. Users, cat. they use the Firefox browser.
3. Users, cat. they use Windows.

Do not worry if some users do not fall into any of these segments, because in real life, data that is difficult to classify often comes across. We simply do not include such users in the sample.

Also, the segments may intersect (after all, it is possible that the user uses Windows, on which Firefox is installed). In order to select segments, you can use the following heuristics (or come up with your own):

|Segment|Heuristics|
|----|----|
|seg_iphone|`parsed_ua['device']['family'] like '%iPhone%'`|
|seg_firefox|`parsed_ua['user_agent']['family'] like '%Firefox%'`|
|seg_windows|`parsed_ua['os']['family'] like '%Windows%'`|

Estimate the number of unique users in each segment using the algorithm [HyperLogLog](https://github.com/svpcom/hyperloglog) (set `error_rate` = 1%).
As a result, output the segments and the number of users in the following format ` 'segment_name <tab> count'. Sort the result by the number of users in descending order.

#### Code for generating batches
In the task, use it without changes because it is critical for the verification system.
```python
from hdfs import Config
import subprocess

client = Config().get_client()
nn_address = subprocess.check_output('hdfs getconf -confKey dfs.namenode.http-address', shell=True).strip().decode("utf-8")

sc = SparkContext(master='yarn-client')

# Preparing base RDD with the input data
DATA_PATH = "/data/realtime/uids"

batches = [sc.textFile(os.path.join(*[nn_address, DATA_PATH, path])) for path in client.list(DATA_PATH)[:30]]

# Creating QueueStream to emulate realtime data generating
BATCH_TIMEOUT = 2 # Timeout between batch generation
ssc = StreamingContext(sc, BATCH_TIMEOUT)
dstream = ssc.queueStream(rdds=batches)
```
