## Task 3. Hadoop MapReduce
 You're to solve 2 problems in this homework, using either [Hadoop Java API](http://hadoop.apache.org/docs/r2.6.1/api) or Hadoop Streaming. Try to optimize your algorithm:
 - use as few jobs as possible
 - use more than 1 reducer (a single reducer is only allowed in final job for sorting the output)

1. [7 points]. Subtask 110 is mandatory for everyone.
2. [14 points]. One of the remaining must be selected according to your username on **gitlab.atp-fivt.org**. Pass the username as an argument to [this](/homeworks/mapreduce_variant_counter.py) script.

Example

```
$ ./get_mr_variant.py alexgabrielsimional
116
```

Commit your code to the corresponding repository on gitlab.atp-fivt.org. Push the first subtask to branch mapreducetask1 and the second one to branch mapreducetask2.

<h3> Deadline </h3>

Friday, September 3, 16.30 BCN

Your solution should pass all the tests before the deadline. After that the code will be manually reviewed. 
You can fix review issues within a day after you recieve them.

### How to submit your code

For each hometask, you've got a repo with branch `master` and a branch for each subtask. To hand in completed work, you must do the following in each branch:

1. Create a directory with the same name as the branch. E.g. directory `mapreducetask1` in branch `mapreducetask1`.

2. This directory should contain a file named `run.sh`, which will serve as the entry point into your program and will be executed by the testing system. `run.sh` can either contain the entire solution or call other files.

> Even if you develop on python, there should be a `run.sh` containing, for instance, just a call to the necessary python script.
>
> 	#!/usr/bin/env bash
> 	
> 	python my_python_script.py $*
>
> Here $* forwards parameters with which `run.sh` was called to `my_python_script.py`.

### Input data

wikipedia:
* location on cluster: whole dataset --- `/data/wiki/en_articles`, sample --- `/data/wiki/en_articles_part`
* format: text, each line looks as follows:
          `article_id <tab> article_text`

list of identifiers:
* location on cluster: whole dataset --- `/data/ids`, sample --- `/data/ids_part`
* format: text, one id per line

### Subtasks

1. (110) Perform a random shuffle of the id list. Then write a random number (1 to 5) of comma-separated ids in each line.
* input: id list
* output format: id1, id2, ...
* print: first 50 lines

For shuffling you could e.g. append a random number to each record, sort the entire list by this new component and then discard it.

Output example:
```
1cf54b530128257d72,4cdf3efa01036a9a48,8c3e7fb30261aaf9cf
4cfe6230016553c3ed,76e1b8690176f801bb,e7409c39013c9db7b4,a5f1519c02b22550e6
83a119ef02346d0879
```

_A real industry case actually. A new database server had to be stress-tested. For that, one needed to generate some realistic-looking queries. The possible set of keys for this DB was known, and also that one query contained up to five keys._

2. (111) Count the occurrences of proper names with lengths of 6 to 9 symbols. A proper name is a word which always occurs in text with the first letter in upper case and the rest in lower case.
While parsing the articles, clean them from punctuation marks. The result must be converted to lower case and sorted by number of occurrences in decreasing order (break ties lexicographically).
* input: wikipedia
* output format: `name <tab> number`
* print: top 10 names

Output example:
```
english 8358
states  7264
british 6829
```

3.(112) (“stop words”) Find words met in the greatest number of documents. Filter out punctuation marks. Sort by number of documents (break ties lexicographically).
* input: wikipedia
* output format: `word <tab> num of documents`
* print: top 10 words

Output example:
```
and	4053
in	4048
to	4045
```

4.(113) Consider an equivalence relation: word1 ~ word2 iff they are permuations of each other (for instance, `abcc` ~ `bcac`). Find the most popular permutations and the number of original words for each permutation. I.e. if `abc` is met 50 times, `bca` is met 50 times, and there are no other permuations of these letters, the result will be `abc 100 2`.
* input: wikipedia
* output format: `permutation <tab> num of occurrences <tab> num of original (unique) words`
* print: top 10 permutations

Output example:
```
for     91296   6
ahtt    81580   2
hitw    79582   2
```

5.(114) Similar to the previous subtask, but you should also count the occurrences of original words. The result should contain the permutation, its popularity and 5 most popular originals. For the same example, the result will be `abc 100 abc:50;bca:50`. Sort the output like in subtask 111. Filter out punctuation marks and words shorter than 3 characters.
* input: wikipedia
* output format: `permutation <tab> num of occurrences <tab> word1:popularity1;…`
* print: top 10 permutations

Output example:
```
asw	91833	was:90214;saw:1604;asw:11;aws:2;wsa:1;
for	89924	for:89912;fro:6;rof:4;rfo:1;ofr:1;
ahtt	81085	that:81085;
```

6.(116) (“bigrams”) Find bigrams (pairs of consecutive words) met in the greatest number of documents. Filter out punctuation marks. Sort by number of documents (break ties lexicographically).
* input: wikipedia
* output format: `word1 word2 <tab> num of documents`
* print: top 10 bigrams

Output example:
```
and the	3495
on the	3326
by the	3250
```

### Hints
1. In all subtasks involving wikipedia articles (111-116), convert the words to lower case before computations.
2. Use the regular expression `[^A-Za-z\\s]` to filter out garbage.
3. By spaces we understand the standard whitespace characters `[ \t\n\r\f\v]` and their contiguous sequences (see the python docs for `string.split()`).
4. The testing system reads the answer from `stdout`, so it shouldn't contain any irrelevant information. `stderr` should be left intact.
